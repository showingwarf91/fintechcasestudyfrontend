/* eslint-disable no-script-url */
import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';

import Title from './Title.js';
import moment from 'moment';
import axios from 'axios';
import Paper from '@material-ui/core/Paper';

// Generate Order Data
function createData(id, date, name, shipTo, paymentMethod, amount) {
  return { id, date, name, shipTo, paymentMethod, amount };
}

const rows = [
  createData(0, '16 Mar, 2019', 'Elvis Presley', 'Tupelo, MS', 'VISA ⠀•••• 3719', 312.44),
  createData(1, '16 Mar, 2019', 'Paul McCartney', 'London, UK', 'VISA ⠀•••• 2574', 866.99),
  createData(2, '16 Mar, 2019', 'Tom Scholz', 'Boston, MA', 'MC ⠀•••• 1253', 100.81),
  createData(3, '16 Mar, 2019', 'Michael Jackson', 'Gary, IN', 'AMEX ⠀•••• 2000', 654.39),
  createData(4, '15 Mar, 2019', 'Bruce Springsteen', 'Long Branch, NJ', 'VISA ⠀•••• 5919', 212.79),
];

const useStyles = makeStyles(theme => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default class Orders extends React.Component {

  constructor() {
    super()
    this.state = {
      countries: []
    }
  }
  async componentWillMount() {
    const forecastUrl = `http://localhost:8000/weather/countries`
    await axios
      .get(forecastUrl)
      .then(result => {
        const countries = result.data;
        var required_countries_at_noon = countries.map((item) => {
          var new_item = item.value.filter((item) => moment.unix(item.dt).utc().hours() === 12)
          return { value: new_item[0], data: item }
        })

        var date_range = [
          {
            range: '-9--1',
            country: required_countries_at_noon.filter((item) => item.value.main.temp >= -9 && item.value.main.temp <= -1)
          },
          {
            range: '0-10',
            country: required_countries_at_noon.filter((item) => item.value.main.temp >= 0 && item.value.main.temp <= 10)
          },
          {
            range: '11-20',
            country: required_countries_at_noon.filter((item) => item.value.main.temp >= 11 & item.value.main.temp <= 21),
          },
          {
            range: '21-30',
            country: required_countries_at_noon.filter((item) => item.value.main.temp >= 21 & item.value.main.temp <= 30),
          },
          {
            range: '31-40',
            country: required_countries_at_noon.filter((item) => item.value.main.temp >= 31 & item.value.main.temp <= 40),
          }
        ]
        this.setState({
          countries: result.data,
          required_countries_at_noon,
          country_date_range: date_range
        });
      })
      .catch(err => {
        this.setState({ loading: false });
        console.log(err);
      });
  }


  getData(country, index) {
    var data = country && country.value.filter((item) => moment.unix(item.dt).utc().hours() === 12)
    if (data) {
      var temp_data = {
        tempeature: data[0].main.temp,
        weather_name: data[0].weather[0].main,
        weather_description: data[0].weather[0].description,
        date_text: data[0].dt_txt
      }

      return (
        <TableRow key={index}>

          <TableCell>{country.name}</TableCell>
          <TableCell>{country.capital}</TableCell>
          <TableCell>{temp_data.tempeature}</TableCell>
          <TableCell>{temp_data.weather_name}</TableCell>
          <TableCell>{temp_data.dt_txt}</TableCell>
        </TableRow>
      )
    }
    return null
  }

  load_filter_data(country, index) {
    if (country) {
      return (
        <TableRow key={index}>
          <TableCell>{country.data.name}</TableCell>
          <TableCell>{country.data.capital}</TableCell>
          <TableCell>{country.value.main.temp}</TableCell>
          <TableCell>{country.value.weather[0].main}</TableCell>
          <TableCell>{country.value.weather[0].description}</TableCell>
          <TableCell>{country.value.dt_txt}</TableCell>
        </TableRow>
      )
    }
  }

  load_degree_with_range(data, index) {
    if (data.country && data.country.length) {
      return (
        <Grid container spacing={2}>
          <Grid item>{data.range}</Grid>
          {data.country.map((item) => <Grid item spacing={2}>
            <Grid container>
              Name:{item.data.name}<br />
              Capital: {item.data.capital}<br />
              Temp: {item.value.main.temp}<br />
              Weather Type: {item.value.weather[0].main}<br />
              Weather Description: {item.value.weather[0].description}<br />
              Time: {item.value.dt_txt}
            </Grid>
          </Grid>)
          }
        </Grid>
      )
    }
  }

  render() {
    return (
      <React.Fragment>
        <Title>Recent Orders</Title>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Degree Range</TableCell>
              <TableCell>Country</TableCell>
              <TableCell>Captial</TableCell>

              <TableCell>Temperature</TableCell>
              <TableCell>Weather</TableCell>
              <TableCell>Weather Main</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.required_countries_at_noon && this.state.required_countries_at_noon.map(this.load_filter_data)}
          </TableBody>
        </Table>
        <div>
          {this.state.country_date_range && this.state.country_date_range.map(this.load_degree_with_range)}
        </div>
      </React.Fragment>
    );
  }
}
