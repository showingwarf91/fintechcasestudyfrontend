import React, { Component } from 'react';
import axios from 'axios';

import _ from 'lodash';
import Title from './Title.js';
import { Grid } from '@material-ui/core';
import { geolocated } from "react-geolocated";
import WeatherIcons from 'react-weathericons';
import SingleLineGridList from './SingleLineGridList';
import moment from 'moment';

class Chart extends Component {
  constructor() {
    super()
    this.state = {
      weather: {},
      current_weather: {},
      loading: false,
    }
  }


  componentWillReceiveProps(nextProps) {
    if (nextProps.isGeolocationAvailable && !this.state.alreadySet && nextProps.coords && nextProps.coords.latitude) {
      this.apiRequest(nextProps)
    }
  }

  apiRequest = async (props) => {
    this.setState({ loading: true });
    const latitude = _.get(props, 'coords.latitude', 50.1121552)
    const longitude = _.get(props, 'coords.longitude', 8.11)

    const currentUrl = `http://localhost:8000/weather/current?lat=${latitude}&lon=${longitude}`
    await axios
      .get(currentUrl)
      .then(result => {
        const currentWeather = result.data;
        this.setState({
          cityName: currentWeather.name,
          temp: currentWeather.main.temp,
          iconId: currentWeather.weather[0].id,
          weather_name: currentWeather.weather[0].main,
          weather_description: currentWeather.weather[0].description,
          humidity: currentWeather.main.humidity,
          time: currentWeather.dt,
          high: currentWeather.main.temp_max,
          low: currentWeather.main.temp_min,
          sunrise: currentWeather.sys.sunrise,
          sunset: currentWeather.sys.sunset,
          alreadySet: true
        });
        this.setTemperatureClass();
      })
      .catch(err => {
        this.setState({ loading: false });
        console.log(err);
      });

    const forecastUrl = `http://localhost:8000/weather/current/forecast?lat=${latitude}&lon=${longitude}`
    await axios
      .get(forecastUrl)
      .then(result => {
        const weatherList = result.data;
        this.setState({
          dataList: result.data.list,
        });
        this.setTemperatureClass();
      })
      .catch(err => {
        this.setState({ loading: false });
        console.log(err);
      });
  }

  // Set temperature class for color combos

  convertTemperature() {
    if (this.state.temperatureUnits === 'metric') {
      return (this.state.temp * 9) / 5 + 32;
    } else {
      return this.state.temp;
    }
  }

  setTemperatureClass() {
    let temp = this.convertTemperature();
    if (temp >= 100) {
      this.setState({
        temperatureClass: 'boiling'
      });
    }
    if (temp < 100 && temp >= 85) {
      this.setState({
        temperatureClass: 'hot'
      });
    }
    if (temp < 85 && temp >= 65) {
      this.setState({
        temperatureClass: 'warm'
      });
    }
    if (temp < 65 && temp >= 50) {
      this.setState({
        temperatureClass: 'perfect'
      });
    }
    if (temp < 50 && temp >= 32) {
      this.setState({
        temperatureClass: 'cool'
      });
    }
    if (temp < 32) {
      this.setState({
        temperatureClass: 'freezing'
      });
    }
  }

  render() {

    var listitems = this.state.dataList
    var current_date = moment()
    // var hours = moment.unix(dt).utc().hours()
    var date_with_hours = listitems && listitems.filter((item) => moment.unix(item.dt).utc().hours() === 12)
    if (this.props.isGeolocationAvailable) {
      return (
        <React.Fragment>
          <Title>Current Weather</Title>
          <Title>Latitude: {this.props.coords && this.props.coords.latitude}</Title>
          <Title>Longitude: {this.props.coords && this.props.coords.longitude}</Title>

          <Grid item xs={12} md={8}>
            <Grid item xs={12} md={8}>
              <Title>Temperature[Celcius] :{this.state.temp}</Title>
              <Title>Weather: {this.state.weather_name}</Title>
              <Title>Weather Description: {this.state.weather_description}</Title>
            </Grid>
          </Grid>
          <Grid container spacing={5}>
            {date_with_hours && date_with_hours.map((data, index) => {
              return (
                <Grid item key={index}>
                  <Title>Date: {data.dt_txt}</Title>
                  <Title>Temperature: {data.main.temp}</Title>
                  <Title>Weather: {data.weather[0].main}</Title>
                  <Title>Weatherdescription: {data.weather[0].description}</Title>
                </Grid>
              )
            })}
          </Grid>
        </React.Fragment>
      );
    }

    else {

      return (
        <React.Fragment>
          <Title>Current Weather</Title>
          <Title>Please Enable the latitude information</Title>
          <Title>Latitude: {this.props.coords && this.props.coords.latitude}</Title>
          <Title>Longitude: {this.props.coords && this.props.coords.longitude}</Title>

          <Grid item xs={12} md={8}>
            <WeatherIcons name="cloud" size="2x" />
          </Grid>
        </React.Fragment>
      );
    }

  }
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(Chart);